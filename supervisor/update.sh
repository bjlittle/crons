#!/usr/bin/env bash

trap 'echo "Aborted!"; exit 1' ERR
set -e

# Set supervisor binary directory and supervisor conf file location.
SUPERVISOR_BIN_DIR="/data/local/avd/miniconda/bin"
SUPERVISOR_CONF_FILE="${BASEDIR}/supervisor/supervisord.conf"

# Capture output to a standard logfile location.
LOG_DIR="${SCRATCH}/logs"
mkdir -p ${LOG_DIR}
LOGFILE="${LOG_DIR}/supervisorctl_update.$(hostname).$(date +%Y-%m-%d-%H%M%S).log"

# Start supervisord process.
${SUPERVISOR_BIN_DIR}/supervisorctl -c ${SUPERVISOR_CONF_FILE} update >> ${LOGFILE} 2>&1

# If anything went wrong or the channel was updated, emit text to produce a
# cron email.
RC=$?

if [ ${RC} -ne 0 -o -s ${LOGFILE} ]; then
    echo "Daemon services updated"
    echo "See logfile : ${LOGFILE}"
    cat ${LOGFILE}
fi
